package com.asavin.moneytracker.Activityes;



import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.asavin.moneytracker.R;
import com.asavin.moneytracker.data.account.Account;
import com.asavin.moneytracker.data.currency.CurrencyesData;
import com.asavin.moneytracker.data.dao.Dao;
import com.asavin.moneytracker.data.trans.Category;
import com.asavin.moneytracker.data.trans.Expence;
import com.asavin.moneytracker.data.trans.Transaction;
import com.asavin.moneytracker.view.list.TransAdapter;


import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
//Баг:при создании транзакцииDao.addTranstoAccount()транзакция заменяет собой все остальные
public class MainActivity extends AppCompatActivity {
    Account account;
    ListView transLV;
    public static ArrayList<Transaction>transactionsAList;
    public static TransAdapter tAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm realm = Realm.getDefaultInstance();

        Expence expence = new Expence(1,10.0, CurrencyesData.UAH);
        RealmList<Transaction> transactions= new RealmList<>();
        Account account = Dao.getAccounts().get(0);//= new Account(1,transes,expence,"name 1");
        Expence money = new Expence(1,30,CurrencyesData.UAH);
        Transaction transaction = new Transaction(0,money,new Category(),new Date(),"name","");
        Dao.addTransactionToAccount(transaction,account);

    }
     void saveActiveAccountId(Account account){
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());;
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt("id",account.getId());
        editor.commit();

    }
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btnAdd:
                startActivity(new Intent(this,AddTransActivity.class));
        }

    }
}
