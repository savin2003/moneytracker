package com.asavin.moneytracker.data.dao;

import com.asavin.moneytracker.data.account.Account;
import com.asavin.moneytracker.data.trans.Transaction;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by 1 on 05.08.2016.
 */
public class Dao {
    public static Realm realm = Realm.getDefaultInstance();
    public static Account createAcount(Account account){
        realm.beginTransaction();

        realm.copyToRealm(account);

        realm.commitTransaction();
        return account;
    }
    public static RealmResults<Account>getAccounts(){
        RealmResults<Account> res = realm.where(Account.class).findAll();
        return res;
    }
    public static void deleteAccounts(){
        RealmResults<Account> accounts = getAccounts();
        for(Account acc:accounts){
            deleteAccount(acc);
        }
    }
    public static Account deleteAccount(Account account){
        account.deleteFromRealm();
        return account;
    }
    public static Transaction addTransactionToAccount(Transaction transaction, Account account){
        realm.beginTransaction();
        account.getTransactions().add(transaction);
        realm.commitTransaction();
        return transaction;
    }

}
