package com.asavin.moneytracker.data.trans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by 1 on 05.08.2016.
 */
public class Category extends RealmObject {
    @PrimaryKey
    int id;
    String type;
    String category;
    String subCategory;

    public Category(int id, String type, String category, String subCategory) {
        this.id = id;
        this.type = type;
        this.category = category;
        this.subCategory = subCategory;
    }

    public Category() {

    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }
}
