package com.asavin.moneytracker.data.trans;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by 1 on 05.08.2016.
 */
public class Expence extends RealmObject{
    public Expence(int id, double money, String currency) {
        this.id = id;
        this.money = money;
        this.currency = currency;
    }

    public Expence() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @PrimaryKey

    int id;
    double money;
    String currency;
}
