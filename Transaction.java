package com.asavin.moneytracker.data.trans;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by 1 on 05.08.2016.
 */
public class Transaction extends RealmObject{
    @PrimaryKey
    int id;
    Expence money;
    Category category;
    Date date;
    String name;
    String description;

    public Transaction(int id, Expence money, Category category, Date date, String name, String description) {
        this.id = id;
        this.money = money;
        this.category = category;
        this.date = date;
        this.name = name;
        this.description = description;
    }

    public Transaction() {

    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Expence getMoney() {
        return money;
    }

    public void setMoney(Expence money) {
        this.money = money;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
