package com.asavin.moneytracker.data.account;

import com.asavin.moneytracker.data.trans.Expence;
import com.asavin.moneytracker.data.trans.Transaction;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by 1 on 05.08.2016.
 */
public class Account extends RealmObject{
    @PrimaryKey
    int id;
    RealmList<Transaction>transactions = new RealmList<>();
    Expence expence;
    String name;

    public Account(int id, RealmList<Transaction> transactions, Expence expence, String name) {
        this.id = id;
        this.transactions = transactions;
        this.expence = expence;
        this.name = name;
    }

    public Account() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Expence getExpence() {
        return expence;
    }

    public void setExpence(Expence expence) {
        this.expence = expence;
    }

    public RealmList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(RealmList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
